(function ($) {

  $(window).load(function () {

    /* Add hover class to Search Submit */
    function add_class() {
      $(this).closest('.search-form').addClass('hover');
    }

    function remove_class() {
      $(this).closest('.search-form').removeClass('hover');
    }
    $('.search-submit').hover(add_class, remove_class);
    $('.search-submit').focusin(add_class);
    $('.search-submit').focusout(remove_class);

    /* Remove Content Area if empty */
    var content_area = $('.content-area');
    if (!content_area.find('img').length && $.trim(content_area.text()) === '') {
      content_area.remove();
    }

    /* Remove Comment Reply if empty */
    $('.comment .reply').each(function () {
      if ($.trim($(this).text()) === '') {
        $(this).remove();
      }
    });

    /* Add class to last column element */
    var child_number = 1;
    $('.column-1-2').filter(':odd').addClass('last-column');
    $('.column-1-3').each(function () {
      if (child_number % 3 === 0) {
        $(this).addClass('last-column');
      }
      child_number++;
    });

    $('.individual-blocks').addClass('OH');

    $('.additional-information').off().on('click', function () {
      var result = $(this).closest('.individual-job-openings');
      result.find('.additional').slideToggle();
    });

    $('.apply').off().on('click', function () {
      window.location.href = $(this).attr("to");
    });

    // Apply for job
    $('.update-gravity-form').click(function () {
      var type = $(this).attr("item");

      $.ajax({
        type: 'POST',
        url: "../wp-admin/admin-ajax.php",
        data: {
          'action': 'my_action',
          'id': $(this).attr("id"),
          'value': $(this).attr("value"),
          'item': $(this).attr("item"),
        },
        success: function (data) {
          console.log("DATA: " + data);

          $("[updateValue='" + type + "']").text(data);

          // Update class
          $("[updateClass='" + type + "']").removeClass();
          var defaultClass = $("[updateClass='" + type + "']").attr("defaultClass");
          console.log("TYPE: " + type);
          console.log("DEFAULT CLASS: " + defaultClass);
          $("[updateClass='" + type + "']").addClass(defaultClass);

          $("[updateClass='" + type + "']").addClass(data);

          // Current Applications
          $('.current-applications').each(function () {
            compareDates(this);
          });
        }
      });
    });

    function grabFilterValues(value) {
      return $('.filter .filter-field.' + value + " select").val();
    }
    console.log("start!");

    console.log(getParameterByName("subject"));

    if (getParameterByName("subject")) {
      console.log(getParameterByName("subject"));
      $('.individual-job-openings[subject!="' + getParameterByName("subject").slice(1,-1) + '"]').hide();
    }

    function getParameterByName(name, url) {
      if (!url) url = window.location.href;
      name = name.replace(/[\[\]]/g, '\\$&');
      var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
      if (!results) return null;
      if (!results[2]) return '';
      return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    // Filter
    $('.page-template-job-openings .filter select').change(function () {
      $('.individual-job-openings').show();

      if (grabFilterValues("subject") !== "all" || getParameterByName("subject")) {
        console.log(grabFilterValues("subject"));
        $('.individual-job-openings[subject!="' + grabFilterValues("subject") + '"]').hide();
      }

      if (grabFilterValues("position") !== "all") {
        $('.individual-job-openings[position!="' + grabFilterValues("position") + '"]').hide();
      }

      if (grabFilterValues("semester") !== "all") {
        $('.individual-job-openings[semester!="' + grabFilterValues("semester") + '"]').hide();
      }

    });

    function compareDates(that) {
      var applicationDate = $(that).attr('class').split(' ')[1];
      var applicationMomentDate = moment(applicationDate, 'YYYY-MM-DD').toDate();
      var currentMomentDate = moment();
      console.log("function run!");

      if (currentMomentDate > applicationMomentDate) {
        $(that).removeClass("future");
        $(that).removeClass("past");
        $(that).addClass("past");
        console.log("past");
      }
      else {
        $(that).removeClass("future");
        $(that).removeClass("past");
        $(that).addClass("future");
        console.log("future");
      }
    }

    $('.current-applications').each(function () {
      compareDates(this);
    });

    $('button.submit-login').off().on('click', function () {
      console.log("click");
      console.log($('input[name="password"]').val());
    });

    // Timetable
    console.log("start timetable");

    if ($(".timetable").length) {
      var timetable = new Timetable();
      timetable.setScope(9, 3)
      timetable.addLocations(['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']);

      $(".timetable-list").each(function () {
        console.log("start foreach");
        let day = $(this).find(".day").text();
        let start = $(this).find(".start").text();
        let end = $(this).find(".end").text();
        let room = $(this).find(".classroom").text().split(" ")[0] || getParameterByName("subject");

        console.log(day);
        console.log(start);
        console.log(end);

        var startDate = moment(start, "hh:mm aa").toDate();
        var endDate = moment(end, "hh:mm aa").toDate()

        if (moment(start, "hh:mm aa").toDate() > moment(end, "hh:mm aa").toDate()) {
          console.log("past");
        }
        else {
          timetable.addEvent(room, day, startDate, endDate, {});
        }
      });

      function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
          results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
      }

      var renderer = new Timetable.Renderer(timetable);

      renderer.draw('.timetable');
    }


  });

})(jQuery);
