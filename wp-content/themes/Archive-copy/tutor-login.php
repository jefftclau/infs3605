<?php
/* Template Name: Tutor Login */

get_header(); ?>
<div id="content" class="site-content">
<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">
    <h1>Log in</h1>
    <?php gravity_form(5, false, false, false, '', false); ?>
  </main>
</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>