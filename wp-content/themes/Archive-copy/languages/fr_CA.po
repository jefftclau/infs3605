# Translation of WordPress.com - Themes - Sequential in French (Canada)
# This file is distributed under the same license as the WordPress.com - Themes - Sequential package.
msgid ""
msgstr ""
"PO-Revision-Date: 2016-08-25 04:39:44+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: GlotPress/2.2.0-alpha\n"
"Language: fr_CA\n"
"Project-Id-Version: WordPress.com - Themes - Sequential\n"

#. translators: %s: Name of current post
#: inc/extras.php:137
msgid "Continue reading %s"
msgstr "Continuer la lecture de %s"

#: inc/template-tags.php:42
msgid "Newer posts"
msgstr "Articles Plus Récents"

#: inc/template-tags.php:30
msgid "Older posts"
msgstr "Articles antérieurs"

#: content.php:25
msgid "Continue reading"
msgstr "Lire la suite"

#: content-grid.php:16
msgid "Read more"
msgstr "Lire la Suite"

#: comments.php:50
msgid "Newer Comments"
msgstr "Commentaires ultérieurs"

#: comments.php:49
msgid "Older Comments"
msgstr "Commentaires antérieurs"

#: inc/customizer.php:20
msgid "Theme Options"
msgstr "Options du thème"

#: archive-jetpack-testimonial.php:23
msgid "Testimonials"
msgstr "Témoignages"

#. Template Name of the plugin/theme
#: wp-content/themes/pub/sequential/page-templates/grid-page.php
msgid "Grid Page"
msgstr "Page Quadrillée"

#. Template Name of the plugin/theme
#: wp-content/themes/pub/sequential/page-templates/full-width-page.php
msgid "Full-Width Page"
msgstr "Page pleine largeur"

#. Template Name of the plugin/theme
#: wp-content/themes/pub/sequential/page-templates/front-page.php
msgid "Front Page"
msgstr "Page d’Accueil"

#: search.php:16
msgid "Search Results for: %s"
msgstr "Résultats de recherche pour : %s"

#: inc/template-tags.php:201
msgid "% Comments"
msgstr "% commentaires"

#: inc/template-tags.php:201
msgid "1 Comment"
msgstr "Un commentaire"

#: inc/template-tags.php:201
msgid "Leave a comment"
msgstr "Laisser un commentaire"

#: inc/template-tags.php:195
msgid "Tagged %1$s"
msgstr "Marqué %1$s"

#: inc/template-tags.php:189
msgid "Posted in %1$s"
msgstr "Posté dans %1$s"

#. translators: used between list items, there is a space after the comma
#: inc/template-tags.php:187 inc/template-tags.php:193
msgid ", "
msgstr ", "

#: inc/template-tags.php:167
msgid "Featured"
msgstr "À la une"

#: inc/template-tags.php:141
msgid "All %s posts"
msgstr "Tous les articles de %s"

#: inc/template-tags.php:79
msgid "<span class=\"meta-nav\">Next Post</span>%title"
msgstr "<span class=\"meta-nav\">Article Suivant</span>%title"

#: inc/template-tags.php:78
msgid "<span class=\"meta-nav\">Previous Post</span>%title"
msgstr "<span class=\"meta-nav\">Article Précédent</span>%title"

#: inc/template-tags.php:72
msgid "<span class=\"meta-nav\">Published In</span>%title"
msgstr "<span class=\"meta-nav\">Publié dans</span>%title"

#: inc/template-tags.php:68
msgid "Post navigation"
msgstr "Navigation des articles"

#: inc/template-tags.php:21
msgid "Posts navigation"
msgstr "Navigation des articles"

#: inc/extras.php:83
msgid "Page %s"
msgstr "Page %s"

#: header.php:45
msgid "Menu"
msgstr "Menu"

#: header.php:23
msgid "Skip to content"
msgstr "Aller au contenu principal"

#. translators: If there are characters in your language that are not supported
#. * by Source Code Pro, translate this to 'off'. Do not translate into your
#. own language.
#: functions.php:186
msgctxt "Source Code Pro font: on or off"
msgid "on"
msgstr "activée"

#. translators: To add an additional Open Sans character subset specific to
#. your language,
#. * translate this to 'greek', 'cyrillic' or 'vietnamese'. Do not translate
#. into your own language.
#: functions.php:154
msgctxt "Open Sans font: add new subset (greek, cyrillic, vietnamese)"
msgid "no-subset"
msgstr "no-subset "

#. translators: If there are characters in your language that are not supported
#. * by Open Sans, translate this to 'off'. Do not translate into your own
#. language.
#: functions.php:148
msgctxt "Open Sans font: on or off"
msgid "on"
msgstr "on"

#. translators: If there are characters in your language that are not supported
#. * by Montserrat, translate this to 'off'. Do not translate into your own
#. language.
#: functions.php:126
msgctxt "Montserrat font: on or off"
msgid "on"
msgstr "activée"

#: functions.php:104
msgid "Footer"
msgstr "Pied de page"

#: functions.php:95
msgid "Sidebar"
msgstr "Colonne latérale"

#: functions.php:60
msgid "Footer Menu"
msgstr "Menu du pied de page"

#: functions.php:59
msgid "Primary Menu"
msgstr "Menu principal"

#: footer.php:32
msgid "Theme: %1$s by %2$s."
msgstr "Thème %1$s par %2$s."

#: footer.php:30
msgid "Proudly powered by %s"
msgstr "Fièrement propulsé par %s"

#: footer.php:30
msgid "http://wordpress.org/"
msgstr "http://wordpress.org/"

#: content-page.php:24 content-single.php:26 content.php:27
msgid "Pages:"
msgstr "Pages :"

#: content-none.php:28
msgid "It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help."
msgstr "Le contenu demandé n&rsquo;a pu être trouvé. Pourquoi ne pas lancer une recherche? "

#: content-none.php:23
msgid "Sorry, but nothing matched your search terms. Please try again with some different keywords."
msgstr "Désolé, mais rien ne correspond à vos critères. Réessayez en modifiant les termes de recherche."

#: content-none.php:19
msgid "Ready to publish your first post? <a href=\"%1$s\">Get started here</a>."
msgstr "Vous êtes prêt à publier votre premier article? <a href=\"%1$s\">Commencez ici</a>."

#: content-none.php:13
msgid "Nothing Found"
msgstr "Inaccessible"

#: content-grid.php:19 content-page.php:32 inc/template-tags.php:175
#: page-templates/front-page.php:26
msgid "Edit"
msgstr "Modifier"

#: comments.php:60
msgid "Comments are closed."
msgstr "Les commentaires sont fermés."

#: comments.php:48
msgid "Comment navigation"
msgstr "Navigation des commentaires"

#: comments.php:29
msgctxt "comments title"
msgid "One thought on &ldquo;%2$s&rdquo;"
msgid_plural "%1$s thoughts on &ldquo;%2$s&rdquo;"
msgstr[0] "Une réflexion sur &ldquo;%2$s&rdquo;"
msgstr[1] "%1$s réflexions sur &ldquo;%2$s&rdquo;"

#: archive.php:66
msgid "Archives"
msgstr "Archives"

#: archive.php:63
msgid "Chats"
msgstr "Clavardages"

#: archive.php:60
msgid "Audios"
msgstr "Audios"

#: archive.php:57
msgid "Statuses"
msgstr "États"

#: archive.php:54
msgid "Links"
msgstr "Liens"

#: archive.php:51
msgid "Quotes"
msgstr "Citations"

#: archive.php:48
msgid "Videos"
msgstr "Vidéos"

#: archive.php:45
msgid "Images"
msgstr "Images"

#: archive.php:42
msgid "Galleries"
msgstr "Galeries"

#: archive.php:39
msgid "Asides"
msgstr "Apartés"

#: archive.php:36
msgctxt "yearly archives date format"
msgid "Y"
msgstr "Y"

#: archive.php:36
msgid "Year: %s"
msgstr "Année&nbsp;: %s"

#: archive.php:33
msgctxt "monthly archives date format"
msgid "F Y"
msgstr "F Y"

#: archive.php:33
msgid "Month: %s"
msgstr "Mois&nbsp;: %s"

#: archive.php:30
msgid "Day: %s"
msgstr "Jour&nbsp;: %s"

#: archive.php:27
msgid "Author: %s"
msgstr "Auteur : %s"

#. translators: %1$s: smiley
#: 404.php:29
msgid "Try looking in the monthly archives. %1$s"
msgstr "Nous vous suggérons de parcourir les archives mensuelles. %1$s"

#: 404.php:20
msgid "It looks like nothing was found at this location. Maybe try one of the links below or a search?"
msgstr "Aucun contenu n&rsquo; a été trouvé à l&rsquo;adresse demandée. Pourquoi ne pas essayer l&rsquo;un des liens ci-dessouss, ou encore, lancer une recherche?"

#: 404.php:16
msgid "Oops! That page can&rsquo;t be found."
msgstr "Oups! Cette page est introuvable."
