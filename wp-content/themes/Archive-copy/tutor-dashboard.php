<?php
/* Template Name: Tutor Dashboard */
// $email = "jeff.lau234@live.com";
$email = $_GET['email'];
// var_dump($email);

get_header(); ?>
<div id="content" class="site-content">
<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">

    <?php
    $form_id = '1';
    $entries = GFAPI::get_entries($form_id);
    $count = 0;

    foreach ($entries as $entry) {
      // var_dump($entry[6]);

      if ($entry[6] == $email) {
        ?>
        <h1>Timetable</h1>

        <div>
          <h3><?php echo $entry["19.3"] ?>'s Timetable</h3>
          <div class="timetable"></div>

          <h3>Class List</h3>

          <?php
          $form_id = '6';
          $entry2 = GFAPI::get_entries($form_id);

          foreach ($entry2 as $timetable) {
            if ($timetable[4] == $entry["id"]) {
              $count = 1;

              ?>
              <div class="timetable-list">
                <strong>
                  <span class="classroom"><?php echo $timetable[8] ?></span> -
                  <span class="day"><?php echo $timetable[7] ?></span> :
                </strong>
                <span class="start"><?php echo $timetable[2] ?></span> to
                <span class="end"><?php echo $timetable[5] ?></span>
              </div>
            <?php
            }
          }
          ?>
        </div>

      <?php
      }
    }
    ?>

    <?php
    if ($count == 0) {
      ?>
      <h1>Application pending</h1>
      <p>Your application is still pending. You'll get an email when your application has been improved.</p>
    <?php
    }
    ?>
    <br /><br />

    <h3>Class Participation</h3>

    <?php
    $form_id = '8';
    $entry3 = GFAPI::get_entries($form_id);
    

    foreach ($entry3 as $participation) {
      // var_dump($participation);

      if ($participation[5] == $email) {
        ?>
        <div class="class-participation">
          <strong><?php echo $participation[1] ?></strong><?php echo " - Student Attendance: "  . $participation[2] . " out of " . $participation[3] ?>
        </div>
      <?php
      }
    }
    ?>
    <br />
    <?php gravity_form(8, false, false, false, '', false); ?>

    <br />
    <h3>Message Administrator</h3>
    <?php gravity_form(9, false, false, false, '', false); ?>


  </main>
</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>