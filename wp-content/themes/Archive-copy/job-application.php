<?php
/* Template Name: Job Application Form */

$entry_id = $_GET['application'];

get_header(); ?>
<div id="content" class="site-content">
<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">
    <header class="entry-header">
      <h1 class="entry-title">
        <?php the_title(); ?>
      </h1>
      <p>Fill in the detail below to apply for a tutor.</p>
    </header>

    <?php
    $form_id = '2';
    $item = GFAPI::get_entry( $entry_id );

      ?>
    <div class="individual-job-openings application">
      <div class="top">
        <?php 
          $date1 = new DateTime($item[10]);
          $date2 = new DateTime("now");
          $interval = $date1->diff($date2);
          $daysleft = $interval->days;
        ?>

        <div class="status"><?php echo 'Closes in ' . $daysleft . ' day' . (($daysleft == 1) ? "": "s"); ?></div>
        <h3><?php echo $item[14] ?></h3>
        <h4 class="role"><?php echo $item[13] ?></h4>
        <h4 class="semester"><?php echo $item[15] ?></h4>
        <h4 class="requirements">Description</h4>
        <p><?php echo wp_strip_all_tags($item[4]) ?></p>
        <div class="additional show">
          <hr />
          <div class="column">
            <h4 class="subheading">Faculty</h4>
            <p><?php echo $item[12] ?></p>
          </div>
          <div class="column">
            <h4 class="subheading">Foundation</h4>
            <p><?php echo $item[11] ?></p>
          </div>
          <div class="column">
            <h4 class="subheading">Lectuer in charge</h4>
            <p><?php echo $item[2] ?></p>
          </div>
          <div class="column">
            <h4 class="subheading">Location</h4>
            <p><?php echo $item[18] ?></p>
          </div>
        </div>
        <hr />
      </div>
    </div>

    <?php if ( have_posts() ) : ?>

    <?php /* Start the Loop */ ?>
    <?php while ( have_posts() ) : the_post(); ?>

    <div class="entry-content">
      <?php
			if ( ! is_search() ) {
				the_content( esc_html__( 'Continue reading', 'sequential' ) );
				wp_link_pages( array(
					'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'sequential' ) . '</span>',
					'after'       => '</div>',
					'link_before' => '<span>',
					'link_after'  => '</span>',
				) );
			} else {
				the_excerpt();
			}
		?>
    </div>

    <?php endwhile; ?>

    <?php sequential_paging_nav(); ?>

    <?php else : ?>

    <?php get_template_part( 'content', 'none' ); ?>

    <?php endif; ?>

  </main><!-- #main -->
</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>