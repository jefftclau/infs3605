<?php
/* Template Name: View Applicants */
$entry_id = $_GET['application'];
$email = $_GET['email'];
$mobile = $_GET['mobile'];

get_header(); ?>
<div id="content" class="site-content">
<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">

    <?php
    $form_id2 = '1';
    $entry2 = GFAPI::get_entry($entry_id);
    $item_applications = GFAPI::get_entry($entry2["15"]);
    // var_dump($item_applications);
    ?>

    <div class="<?php echo "current-applications " . $item_applications['10'] ?>" updateClass="10" defaultClass="current-applications" />
      <h3><?php echo $item_applications[14] ?></h3>
      <h4 class="role"><?php echo $item_applications[13] ?></h4>
      <h4 class="semester"><?php echo $item_applications[15] ?></h4>
      <h4 class="requirements">Description</h4>
      <p><?php echo wp_strip_all_tags($item_applications[4]) ?></p>

      <div class="additional show">
        <div class="column">
          <h4 class="subheading">Faculty</h4>
          <p><?php echo $item_applications[12] ?></p>
        </div>
        <div class="column">
          <h4 class="subheading">Foundation</h4>
          <p><?php echo $item_applications[11] ?></p>
        </div>
        <div class="column">
          <h4 class="subheading">Lectuer in charge</h4>
          <p><?php echo $item_applications[2] ?></p>
        </div>
        <div class="column">
          <h4 class="subheading">Location</h4>
          <p><?php echo $item_applications[18] ?></p>
        </div>
        <div class="column">
          <h4 class="subheading">Expiry Date</h4>
          <p><?php echo $item_applications[10] ?></p>
        </div>
      </div>
    </div>

<?php
$form_id = '1';
$entry = GFAPI::get_entry($entry_id);
?>
<div class="view-applicant">
  <div class="<?php echo 'current-status ' . $entry["approval_status"] ?>" updateClass="approval_status" defaultClass="current-status">
    <div class="n1">
      <h3>Approved</h3>
      <p>This applicant has been approved. He will be sent an success email with all the details to start tutoring</p>
    </div>
    <div class="n2">
      <h3>Rejected</h3>
      <p>This applicant has been rejected. He will be sent an rejection email.</p>
    </div>
    <div class="n3">
      <h3>Pending</h3>
      <p>This applicant is still pending. After approving a applicant they will have recieve a success email with all the deatils to start tutoring</h2>
    </div>
  </div>
  <h2>Applicant Details</h2>
  <div class="column double">
    <div class="subheading">Approval Status</div>
    <p updateValue="approval_status"><?php echo ucfirst($entry["approval_status"]) ?></p>
  </div>

  <div class="column">
    <div class="subheading">Applicant</div>
    <p><?php echo $entry["19.3"] . " " . $entry["19.6"] ?></p>
  </div>
  <div class="column">
    <div class="subheading">Email</div>
    <p><?php echo $entry["6"] ?></p>
  </div>

  <div class="column">
    <div class="subheading">Address</div>
    <p><?php echo $entry["18.1"] . ' ' . $entry["18.2"] . ', ' . $entry["18.3"] . ', ' . $entry["18.4"] . ', ' . $entry["18.5"] . ', ' . $entry["18.6"] ?></p>
  </div>

  <div class="column">
    <div class="subheading">Home Phone</div>
    <p><?php echo $entry[7] ?></p>
  </div>

  <div class="column">
    <div class="subheading">Mobile Phone</div>
    <p><?php echo $entry[20] ?></p>
  </div>

  <div class="column">
    <div class="subheading">WAM</div>
    <p><?php echo $entry[12] ?></p>
  </div>

  <div class="column">
    <div class="subheading">Past Experience</div>
    <p><?php echo $entry["13.1"] ?></p>
  </div>

  <div class="column">
    <div class="subheading">Description of experience</div>
    <p><?php echo $entry[14] ?></p>
  </div>
</div>

<hr />

<h2>Message</h2>
<?php gravity_form(7, false, false, false, '', false); ?>

<hr />

<script>
  function goBack() {
    window.history.back();
  }
</script>

<a class="button back" onclick="goBack()">Back</a>
<button id="<?php echo $entry_id; ?>" item="approval_status" value="approved" class="update-gravity-form" method="POST">Approve</button>
<button id="<?php echo $entry_id; ?>" item="approval_status" value="reject" class="update-gravity-form" method="POST">Reject</button>

</main><!-- #main -->
</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>