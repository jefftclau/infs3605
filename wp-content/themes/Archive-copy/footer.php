<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Sequential
 */
?>

	</div><!-- #content -->

  <?php get_sidebar( 'footer' ); ?>
  
	<?php if ( has_nav_menu( 'footer' ) ) : ?>
		<nav class="footer-navigation" role="navigation">

			<?php
				wp_nav_menu( array(
					'theme_location'  => 'footer',
					'container_class' => 'menu-footer',
					'menu_class'      => 'clear',
					'depth'           => 1,
				) );
			?>
		</nav><!-- #site-navigation -->
	<?php endif; ?>

	<footer id="colophon" class="site-footer" role="contentinfo">
  inUNSW, UNSW Sydney NSW 2052 Australia. Provider Code: 00098G ABN: 57 195 873 179
  </footer><!-- #colophon -->
  
</div><!-- #page -->

<?php wp_footer(); ?>

<script src="https://cdn.jsdelivr.net/momentjs/2.13.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.css">

</body>
</html>