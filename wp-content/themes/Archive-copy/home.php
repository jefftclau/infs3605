<?php
/* Template Name: Home */

get_header(); ?>

<div class="image-home">

<div id="content" class="site-content">
  <h1>Join the <span class="text-yellow">most prestigious university</span></h1>
  <p>Join UNSW's team of tutors. Not sure what you are looking for, sort by subject, position and semester below.</p>
</div>

<?php
    $form_id = '2';
    $form = GFAPI::get_form($form_id);
    ?>
    <div class="search-box">
      <h3>Search our jobs</h3>
      <div class="filter">
        <div class="filter-field subject">
          <label>Subject</label>
          <select>
            <option value="all" selected>All subjects</option>
            <?php
            $subjects = $form["fields"][0]["choices"];
            foreach ($subjects as $subjects_item) {
              ?>
              <option value="<?php echo $subjects_item["value"] ?>"><?php echo $subjects_item["text"] ?></option>
            <?php
            }
            ?>
          </select>
        </div>
        <div class="filter-field position">
          <label>Position</label>
          <select>
            <option value="all" selected>All positions</option>
            <?php
            $subjects = $form["fields"][5]["choices"];

            foreach ($subjects as $subjects_item) {
              ?>
              <option value="<?php echo $subjects_item["value"] ?>"><?php echo $subjects_item["text"] ?></option>
            <?php
            }
            ?>
          </select>
        </div>
        <div class="filter-field semester">
          <label>Semester</label>
          <select>
            <option value="all" selected>All semesters</option>
            <?php
            $subjects = $form["fields"][6]["choices"];

            foreach ($subjects as $subjects_item) {
              ?>
              <option value="<?php echo $subjects_item["value"] ?>"><?php echo $subjects_item["text"] ?></option>
            <?php
            }
            ?>
          </select>
        </div>
        <a href="/job-openings/" style="text-align: center;" class="button submit">Search now!</a>
      </div>
    </div>
</div>
<div id="content" class="site-content">
  
<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">
    <?php
    $form_id = '2';
    $form = GFAPI::get_form($form_id);
    ?>
    <!-- <div class="search-box">
      <div class="filter">
        <h1>Join the <span class="text-yellow">most prestigious university</span></h1>
        <p>Join UNSW's team of tutors. Not sure what you are looking for, sort by subject, position and semester below.</p>
        <div class="filter-field subject">
          <label>Subject</label>
          <select>
            <option value="all" selected>All subjects</option>
            <?php
            $subjects = $form["fields"][0]["choices"];
            foreach ($subjects as $subjects_item) {
              ?>
              <option value="<?php echo $subjects_item["value"] ?>"><?php echo $subjects_item["text"] ?></option>
            <?php
            }
            ?>
          </select>
        </div>
        <div class="filter-field position">
          <label>Position</label>
          <select>
            <option value="all" selected>All positions</option>
            <?php
            $subjects = $form["fields"][5]["choices"];

            foreach ($subjects as $subjects_item) {
              ?>
              <option value="<?php echo $subjects_item["value"] ?>"><?php echo $subjects_item["text"] ?></option>
            <?php
            }
            ?>
          </select>
        </div>
        <div class="filter-field semester">
          <label>Semester</label>
          <select>
            <option value="all" selected>All semesters</option>
            <?php
            $subjects = $form["fields"][6]["choices"];

            foreach ($subjects as $subjects_item) {
              ?>
              <option value="<?php echo $subjects_item["value"] ?>"><?php echo $subjects_item["text"] ?></option>
            <?php
            }
            ?>
          </select>
        </div>
        <a href="/job-openings/" style="text-align: center;" class="button submit">Search now!</a>
      </div>
    </div> -->
    <div class="latest-opening">
      <h3>Latest Opening at UNSW</h3>
      <?php
      $entry = GFAPI::get_entries($form_id);
      $item = $entry[0];
      // var_dump($entry[0]);
      ?>
      <div class="individual-job-openings" subject="<?php echo $item[14] ?>" position="<?php echo $item[13] ?>" semester="<?php echo $item[15] ?>">
        <div class="top">
          <?php
          $date1 = new DateTime($item[10]);
          $date2 = new DateTime("now");
          $interval = $date1->diff($date2);
          $daysleft = $interval->days;
          ?>

          <div class="status"><?php echo 'Closes in ' . $daysleft . ' day' . (($daysleft == 1) ? "" : "s"); ?></div>
          <h3><?php echo $item[14] ?></h3>
          <h4 class="role"><?php echo $item[13] ?></h4>
          <h4 class="semester"><?php echo $item[15] ?></h4>
          <h4 class="requirements">Description</h4>
          <p><?php echo wp_strip_all_tags($item[4]) ?></p>
          <div class="additional">
            <hr />
            <div class="column">
              <h4 class="subheading">Faculty</h4>
              <p><?php echo $item[12] ?></p>
            </div>
            <div class="column">
              <h4 class="subheading">Foundation</h4>
              <p><?php echo $item[11] ?></p>
            </div>
            <div class="column">
              <h4 class="subheading">Lectuer in charge</h4>
              <p><?php echo $item[2] ?></p>
            </div>
            <div class="column">
              <h4 class="subheading">Location</h4>
              <p><?php echo $item[18] ?></p>
            </div>
          </div>
          <hr />
          <button class="apply" id="<?php echo $item["id"] ?>" to="<?php echo get_home_url() . '/job-application/?application=' . $item["id"] ?>">Apply now!</button>
        </div>
        <div class="additional-information">View more details</div>

      </div>
    </div>
    <div class="subjects-home">
      <h3>Apply for subjects for Information Systems</h3>
      <p>Choose from a selection of subjects.</p>
      <h5>1st Year Subjects</h5>
      <ul>
        <?php
        $subjects = $form["fields"][0]["choices"];
        foreach ($subjects as $subjects_item) {
          if (strpos($subjects_item["text"], 'INFS1') !== false) {
            ?>
            <li><a href="<?php echo "/job-openings/?subject='" . $subjects_item["text"] . "'" ?>"><span><?php echo $subjects_item["text"] ?></span></a></li>
          <?php
          }
        }
        ?>
      </ul>
      <h5>2nd Year Subjects</h5>
      <ul>
        <?php
        $subjects = $form["fields"][0]["choices"];
        foreach ($subjects as $subjects_item) {
          if (strpos($subjects_item["text"], 'INFS2') !== false) {
            ?>
            <li><a href="<?php echo "/job-openings/?subject='" . $subjects_item["text"] . "'" ?>"><span><?php echo $subjects_item["text"] ?></span></a></li>
          <?php
          }
        }
        ?>
      </ul>
      <h5>3rd Year Subjects</h5>
      <ul>
        <?php
        $subjects = $form["fields"][0]["choices"];
        foreach ($subjects as $subjects_item) {
          if (strpos($subjects_item["text"], 'INFS3') !== false) {
            ?>
            <li><a href="<?php echo "/job-openings/?subject='" . $subjects_item["text"] . "'" ?>"><span><?php echo $subjects_item["text"] ?></span></a></li>
          <?php
          }
        }
        ?>
      </ul>
      <h5>4th Year Subjects</h5>
      <ul>
        <?php
        $subjects = $form["fields"][0]["choices"];
        foreach ($subjects as $subjects_item) {
          if (strpos($subjects_item["text"], 'INFS4') !== false) {
            ?>
            <li><a href="<?php echo "/job-openings/?subject='" . $subjects_item["text"] . "'" ?>"><span><?php echo $subjects_item["text"] ?></span></a></li>
          <?php
          }
        }
        ?>
      </ul>
    </div>

    <div class="quick-links">
      <h3>Why work at UNSW?</h3>
      <div class="image-campus">
      </div>
      <p>
        The University of New South Wales is committed to providing a working environment where our employees are supported to do their best work to enable them to develop a long and successful career.
      </p>
      <p>
        Our people are at the centre of this commitment. At UNSW you will have the opportunity to work with world class academics, supported by outstanding professional and technical staff in modern research and teaching facilities. In addition, the University provides a diverse range of professional development opportunities and employee benefits for staff, as well as opportunities to enjoy an array of concerts, workshops and public lectures both on and off-campus.
      </p>
      <p>
        By joining us at the University of New South Wales, you will be working with some of the world’s leading scholars to make a real difference.
      </p>


    </div>

  </main><!-- #main -->
</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>