<?php
/* Template Name: Approved Tutors*/

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

get_header(); ?>
<div id="content" class="site-content">
<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">
    <h1>Approved Tutors</h1>
    <!-- <hr /> -->
    <br />
    <?php while (have_posts()) : the_post(); ?>

      <?php // get_template_part('content', 'page'); 
      ?>

      <?php
      // If comments are open or we have at least one comment, load up the comment template
      if (comments_open() || '0' != get_comments_number()) :
        comments_template();
      endif;
      ?>

    <?php endwhile; // end of the loop. 
    ?>

    <?php
    $form_id = '1';
    $entry = GFAPI::get_entries($form_id);

    $check_array = array();
    foreach ($entry as $item) {
      // var_dump($item);
      if (in_array($item['15'], $check_array)) {
        continue;
      }
      $check_array[] = $item['15'];
      // var_dump($item["15"]);
      ?>
      <?php
      $item_applications = GFAPI::get_entry($item["15"]);
      $date1 = new DateTime($item_applications[10]);
      $date2 = new DateTime("now");
      $interval = $date1->diff($date2);
      $daysleft = $interval->days;

      // var_dump($item_applications);
      ?>
      <?php
      // $value = 0;
      foreach ($entry as $item_secondary) {
        if ($item[15] == $item_secondary[15] && $item_secondary["approval_status"] == "approved") {
          // var_dump($value);
          // $value = $value + 1;
          // var_dump($value);
          ?>
          <div class="<?php echo "current-applications " . $item_applications['10'] ?>" updateClass="10" defaultClass="current-applications" />
          <h3><?php echo $item_applications[14] ?></h3>
          <h4 class="role"><?php echo $item_applications[13] ?></h4>
          <h4 class="semester"><?php echo $item_applications[15] ?></h4>

          <div class="additional show">
            <div class="column">
              <h4 class="subheading">Name</h4>
              <p><?php echo $item_secondary["19.3"] . " " . $item_secondary["19.6"] ?></p>
            </div>
            <div class="column">
              <h4 class="subheading">Faculty</h4>
              <p><?php echo $item_applications[12] ?></p>
            </div>
            <div class="column">
              <h4 class="subheading">Foundation</h4>
              <p><?php echo $item_applications[11] ?></p>
            </div>
            <div class="column">
              <h4 class="subheading">Lectuer in charge</h4>
              <p><?php echo $item_applications[2] ?></p>
            </div>
            <div class="column">
              <h4 class="subheading">Location</h4>
              <p><?php echo $item_applications[18] ?></p>
            </div>
            <div class="column">
              <h4 class="subheading">Email</h4>
              <p><?php echo $item_secondary["6"] ?></p>
            </div>
            <div class="column">
              <h4 class="subheading">Status</h4>
              <p><?php echo ucfirst($item_secondary["approval_status"]) ?></p>
            </div>
            <div class="navigation">
              <!-- <button></button> -->
              <button class="apply" id="<?php echo $item_secondary["id"] ?>" to="<?php echo get_home_url() . '/assign-timetable/?application=' . $item_secondary["id"] . '&email=' . $item_secondary[6] . '&mobile='  . $item_secondary[20] . '&subject=' . explode(' ', trim($item_applications[14]))[0] . '' ?>">Assign <?php echo $item_secondary["19.3"]?>'s Class</button>
            </div>
          </div>
        <?php
        }
        ?><?php
            }

            ?>

  </div>
<?php
}
?>

</main><!-- #main -->
</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>