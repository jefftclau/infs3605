<?php
/* Template Name: Job Openings */

get_header(); ?>
<div id="content" class="site-content">
<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">

    <?php while (have_posts()) : the_post(); ?>

    <?php get_template_part('content', 'page'); ?>

    <?php
      // If comments are open or we have at least one comment, load up the comment template
      // https://docs.gravityforms.com/getting-started-gravity-forms-api-gfapi/#updating-a-form
      if (comments_open() || '0' != get_comments_number()) :
        comments_template();
      endif;
      ?>

    <?php endwhile; // end of the loop. 
    ?>

    <?php 
    
    $form_id = '2';
    $form = GFAPI::get_form( $form_id );

    // var_dump($form["fields"][5]);
    ?>

    <hr />
    <div class="filter">
      <h5>Filter</h5>
      <div class="filter-field subject">
        <label>Subject</label>
        <select>
          <option value="all" selected>All subjects</option>
          <?php
          $subjects = $form["fields"][0]["choices"];
          foreach ($subjects as $subjects_item) {
          ?>
          <option value="<?php echo $subjects_item["value"] ?>"><?php echo $subjects_item["text"]?></option>
          <?php
          }
        ?>
        </select>
      </div>
      <div class="filter-field position">
        <label>Position</label>
        <select>
          <option value="all" selected>All positions</option>
          <?php
          $subjects = $form["fields"][5]["choices"];
          
          foreach ($subjects as $subjects_item) {
          ?>
          <option value="<?php echo $subjects_item["value"] ?>"><?php echo $subjects_item["text"]?></option>
          <?php
          }
        ?>
        </select>
      </div>
      <div class="filter-field semester">
        <label>Semester</label>
        <select>
          <option value="all" selected>All semesters</option>
          <?php
          $subjects = $form["fields"][6]["choices"];
          
          foreach ($subjects as $subjects_item) {
          ?>
          <option value="<?php echo $subjects_item["value"] ?>"><?php echo $subjects_item["text"]?></option>
          <?php
          }
        ?>
        </select>
      </div>
    </div>
    <hr />

    <?php
    $entry = GFAPI::get_entries($form_id);


    foreach ($entry as $item) {
      ?>
    <div class="individual-job-openings" subject="<?php echo $item[14] ?>" position="<?php echo $item[13] ?>" semester="<?php echo $item[15] ?>">
      <div class="top">
        <?php 
          $date1 = new DateTime($item[10]);
          $date2 = new DateTime("now");
          $interval = $date1->diff($date2);
          $daysleft = $interval->days;
        ?>

        <div class="status"><?php echo 'Closes in ' . $daysleft . ' day' . (($daysleft == 1) ? "": "s"); ?></div>
        <h3><?php echo $item[14] ?></h3>
        <h4 class="role"><?php echo $item[13] ?></h4>
        <h4 class="semester"><?php echo $item[15] ?></h4>
        <h4 class="requirements">Description</h4>
        <p><?php echo wp_strip_all_tags($item[4]) ?></p>
        <div class="additional">
          <hr />
          <div class="column">
            <h4 class="subheading">Faculty</h4>
            <p><?php echo $item[12] ?></p>
          </div>
          <div class="column">
            <h4 class="subheading">Foundation</h4>
            <p><?php echo $item[11] ?></p>
          </div>
          <div class="column">
            <h4 class="subheading">Lectuer in charge</h4>
            <p><?php echo $item[2] ?></p>
          </div>
          <div class="column">
            <h4 class="subheading">Location</h4>
            <p><?php echo $item[18] ?></p>
          </div>
        </div>
        <hr />
        <button class="apply" id="<?php echo $item["id"] ?>" to="<?php echo get_home_url() . '/job-application/?application=' . $item["id"] ?>">Apply now!</button>
      </div>
      <div class="additional-information">View more details</div>
      
    </div>
    <?php
    }
    ?>

  </main><!-- #main -->
</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>