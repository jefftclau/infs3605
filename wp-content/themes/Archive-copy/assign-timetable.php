<?php
/* Template Name: Assign Timetable */
$entry_id = $_GET['application'];
$email = $_GET['email'];
$mobile = $_GET['mobile'];

get_header(); ?>
<div id="content" class="site-content">
<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">

    <?php
    $form_id = '1';
    $entry = GFAPI::get_entry($entry_id);
    // var_dump($entry);
    ?>
    <h1>Register Timetable</h1>

    <div>
      <p>Specify the time and place for <?php echo $entry["19.3"] . " " . $entry["19.6"] ?></p>
      <h3><?php echo $entry["19.3"] ?>'s Timetable</h3>
      <div class="timetable"></div>

      <h3>Class List</h3>

      <?php
      $form_id = '6';
      $entry = GFAPI::get_entries($form_id);
      // var_dump($entry);

      foreach ($entry as $timetable) {

        if ($timetable[4] == $entry_id) {
          ?>
          <div class="timetable-list">
            <strong>
              <span class="classroom"><?php echo $timetable[8] ?></span> -
              <span class="day"><?php echo $timetable[7] ?></span> :
            </strong>
            <span class="start"><?php echo $timetable[2] ?></span> to
            <span class="end"><?php echo $timetable[5] ?></span>
          </div>
        <?php
        }
      }
      ?>

      <?php gravity_form(6, false, false, false, '', false); ?>

    </div>
    <hr />
    <h2>Message</h2>
    <?php gravity_form(7, false, false, false, '', false); ?>

    <hr />

    <script>
      function goBack() {
        window.history.back();
      }
    </script>

    <a class="button back" onclick="goBack()">Back</a>

  </main>
</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>