<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'infs' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '%(EiAd%QRBBPf.)Ab7W2V|g^5(8EUtSE:$z<Bd&65G{yU@#1qrz8j!X)dUp%=7]7' );
define( 'SECURE_AUTH_KEY',  'D67Z8.r@<K0/-}@=nM2-Q8I4HD0[:fOPK]1diX3-z494wV(6)[*SV#cv3Je9&R~x' );
define( 'LOGGED_IN_KEY',    'P2rn#G8?]py>xXO3ah*pAIyc`FLr^*B_kDaJOC4n@GNUcv9=^TGB,JQztr:i.YGy' );
define( 'NONCE_KEY',        ';; MXlT6g2. [%raK=;&S/abz#:P~axeF.FTY^&Ugo#8iT`z}]252lU5q:::S`w|' );
define( 'AUTH_SALT',        'D]--Fc|t;of,BUr=a>)]l!# NUO|i*NbZ_$p`ga<vWY*&|m-$OgdfVv#q.yg/Tc,' );
define( 'SECURE_AUTH_SALT', '`#+>Pz=) #10iO+t@xItnhZ >+&&@9n3jM{9T}:-II%KhyLab9=+>3%dB7bM`m;c' );
define( 'LOGGED_IN_SALT',   '^:D)|b/n?fE;.3]$:?Kr9wMAksBAX(W(T4!mSJPrZ9w(HH=,e;lL368O=y?z_!(N' );
define( 'NONCE_SALT',       'l,E.4 rXhEFGPEXikfEmrr)Q=tNk&D~~KAgLb]*xbQtXNAo |/1+[xszli({@1?(' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
